using System.Collections.Generic;
using UnityEngine;

public class Vehicle : MonoBehaviour
{
    public static List<Vehicle> allVehicles = new List<Vehicle>();

    [SerializeField] Transform target;
    [SerializeField] float mass = 200f;
    [SerializeField] float maxSpeed = 10f;
    [SerializeField] float maxForce = 4f;

    SteeringBehaviour[] steeringBehaviours;
    Vector3 velocity;

    void Awake()
    {
        allVehicles.Add(this);
        steeringBehaviours = GetComponents<SteeringBehaviour>();
    }

    void Update()
    {
        Vector2 steeringForce = Vector2.zero;
        for (int i = 0; i < steeringBehaviours.Length; i++)
        {
            steeringForce += steeringBehaviours[i].Steer(target, velocity, maxSpeed, maxForce);
        }
        steeringForce = Vector2.ClampMagnitude(steeringForce, maxForce);
        Vector2 acceleration = steeringForce / mass;
        velocity += acceleration.To3d();
        if (velocity.magnitude > 0f)
            transform.Translate(velocity * Time.deltaTime);
    }
}
