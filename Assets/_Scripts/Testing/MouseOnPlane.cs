using UnityEngine;

public class MouseOnPlane : MonoBehaviour
{
    Camera cam;
    Plane plane;

    void Awake()
    {
        cam = Camera.main;
        plane = new Plane(Vector3.up, Vector3.zero);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
    }

    void Update()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        float distance;
        if (plane.Raycast(ray, out distance))
        {
            Vector3 pointOnPlane = ray.origin + ray.direction * distance;
            transform.position = pointOnPlane;
        }
    }
}
