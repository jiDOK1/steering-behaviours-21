using UnityEngine;

public class FleeMouseSimple : MonoBehaviour
{
    [SerializeField] float maxSpeed = 1f;
    [SerializeField] float maxForce = 1f;
    [SerializeField] float mass = 5f;
    Transform target;
    Vector3 velocity;
    void Awake()
    {
        target = GameObject.Find("MouseObject").transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 desired = transform.position.To2d() - target.position.To2d();
        desired = desired.normalized * maxSpeed;
        Vector2 steeringForce = desired - velocity.To2d();
        steeringForce = Vector2.ClampMagnitude(steeringForce, maxForce);
        Vector2 acceleration = steeringForce / mass;
        velocity += acceleration.To3d();
        transform.Translate(velocity * Time.deltaTime);
    }
}
