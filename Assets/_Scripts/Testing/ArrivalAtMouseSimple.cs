using UnityEngine;

public class ArrivalAtMouseSimple : MonoBehaviour
{
    [SerializeField] float slowingDistance = 1f;
    [SerializeField] float maxSpeed = 1f;
    [SerializeField] float maxForce = 1f;
    [SerializeField] float mass = 5f;
    Transform target;
    Vector3 velocity;

    void Awake()
    {
        target = GameObject.Find("MouseObject").transform;
    }

    void Update()
    {
        // Steering
        Vector3 targetOffset = target.position - transform.position;
        float distance = targetOffset.magnitude;
        float rampedSpeed = maxSpeed * (distance / slowingDistance);
        float clippedSpeed = Mathf.Min(rampedSpeed, maxSpeed);
        Vector3 desired = (clippedSpeed / distance) * targetOffset;
        Vector3 steeringForce = desired - velocity;
        steeringForce = Vector3.ClampMagnitude(steeringForce, maxForce);
        // Locomotion
        Vector3 acceleration = steeringForce / mass;
        velocity += acceleration;
        transform.Translate(velocity * Time.deltaTime);
    }
}
