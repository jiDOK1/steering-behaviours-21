using UnityEngine;

public class SeekMouseSimple : MonoBehaviour
{
    [SerializeField] float maxSpeed = 1f;
    [SerializeField] float maxForce = 1f;
    [SerializeField] float mass = 5f;
    Transform target;
    Vector3 velocity;

    void Awake()
    {
        target = GameObject.Find("MouseObject").transform;
    }

    void Update()
    {
        // Steering
        Vector2 desired = target.position.To2d() - transform.position.To2d();
        desired = desired.normalized * maxSpeed;
        Vector2 steeringForce = desired - velocity.To2d();
        steeringForce = Vector2.ClampMagnitude(steeringForce, maxForce);
        // Locomotion
        Vector2 acceleration = steeringForce / mass;
        velocity += acceleration.To3d();
        transform.Translate(velocity * Time.deltaTime);
        Visualize(desired, steeringForce, velocity);
    }

    void Visualize(Vector2 desired, Vector2 steeringForce, Vector3 velocity)
    {
        Debug.DrawRay(transform.position, desired.To3d(), Color.red);
        Debug.DrawRay(transform.position + velocity, steeringForce.To3d(), Color.green);
        Debug.DrawRay(transform.position, velocity, Color.blue);
    }
}
