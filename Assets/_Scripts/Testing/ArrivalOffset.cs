using UnityEngine;

public class ArrivalOffset : MonoBehaviour
{
    [SerializeField] float stopDistance = 2f;
    [SerializeField] float slowingDistance = 7f;
    [SerializeField] float maxSpeed = 1f;
    [SerializeField] float maxForce = 1f;
    [SerializeField] float mass = 5f;
    Transform target;
    Vector3 velocity;

    void Awake()
    {
        target = GameObject.Find("MouseObject").transform;
    }

    void Update()
    {
        // Steering
        Vector3 dirToTarget = target.position - transform.position;
        float distance = dirToTarget.magnitude;
        dirToTarget = Vector3.ClampMagnitude(dirToTarget, distance - stopDistance);
        distance = dirToTarget.magnitude;
        Vector3 desired = Mathf.Lerp(0f, maxSpeed, distance / slowingDistance) * dirToTarget.normalized;
        Vector3 steeringForce = desired - velocity;
        steeringForce = Vector3.ClampMagnitude(steeringForce, maxForce);
        // Locomotion
        Vector3 acceleration = steeringForce / mass;
        velocity += acceleration;
        transform.Translate(velocity * Time.deltaTime);
    }
}
