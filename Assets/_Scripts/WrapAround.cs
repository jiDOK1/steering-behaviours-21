using UnityEngine;

public class WrapAround : MonoBehaviour
{
    float minX;
    float maxX;
    float minZ;
    float maxZ;

    void Awake()
    {
        Transform floor = GameObject.Find("Floor").transform;
        Mesh mesh = floor.GetComponent<MeshFilter>().mesh;
        maxX = (mesh.bounds.size.x / 2) * floor.localScale.x;
        minX = -maxX;
        maxZ = (mesh.bounds.size.z / 2) * floor.localScale.z;
        minZ = -maxZ;
        maxX += floor.position.x;
        minX += floor.position.x;
        maxZ += floor.position.z;
        minZ += floor.position.z;
    }
        
    void Update()
    {
        if (transform.position.x > maxX)
        {
            float newX = minX + (transform.position.x - maxX);
            transform.position = new Vector3(newX, transform.position.y, transform.position.z);
        }
        if (transform.position.x < minX)
        {
            float newX = maxX - (transform.position.x + minX);
            transform.position = new Vector3(newX, transform.position.y, transform.position.z);
        }
        if (transform.position.z > maxZ)
        {
            float newZ = minZ + (transform.position.z - maxZ);
            transform.position = new Vector3(transform.position.x, transform.position.y, newZ);
        }
        if (transform.position.z < minZ)
        {
            float newZ = maxZ - (transform.position.z + minZ);
            transform.position = new Vector3(transform.position.x, transform.position.y, newZ);
        }
    }
}
