using UnityEngine;

public class Containment : SteeringBehaviour
{
    [SerializeField] float probeDist = 4f;
    [SerializeField] float probeRadius = 1f;

    public override Vector2 Steer(
        Transform target, 
        Vector3 velocity, 
        float maxSpeed, 
        float maxForce)
    {
        // probe point
        Vector3 probePoint = velocity.To2d().normalized * probeDist;
        // check for collision
        bool isInWall = Physics.CheckSphere(probePoint, probeRadius);
        if (isInWall)
        {
            // project on wall (how?)
            // get normal at that point
            // "Steering is determined by taking the component of this surface normal which is 
            // perpendicular to the vehicle's forward direction"
        }

        return Vector2.zero;
    }
}
