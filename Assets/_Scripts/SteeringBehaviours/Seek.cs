using UnityEngine;

public class Seek : SteeringBehaviour
{
    public override Vector2 Steer(
        Transform target, 
        Vector3 velocity, 
        float maxSpeed, 
        float maxForce)
    {
        Vector2 desired = target.position.To2d() - transform.position.To2d();
        desired = desired.normalized * (maxSpeed + speedModifier);
        Vector2 steeringForce = desired - velocity.To2d();
        steeringForce = Vector2.ClampMagnitude(steeringForce, maxForce + forceModifier);
        return steeringForce;
    }
}
