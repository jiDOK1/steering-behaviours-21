using UnityEngine;

public class Separation : SteeringBehaviour
{
    [SerializeField] float maxSepDist = 3f;
    public override Vector2 Steer(
        Transform target, 
        Vector3 velocity, 
        float maxSpeed, 
        float maxForce)
    {
        Vector2 desired = Vector2.zero;
        int count = 0;
        for (int i = 0; i < Vehicle.allVehicles.Count; i++)
        {
            Vector2 desiredSep = transform.position.To2d() - Vehicle.allVehicles[i].transform.position.To2d();
            float distToVehicle = desiredSep.magnitude;
            if (distToVehicle > maxSepDist || Vehicle.allVehicles[i] == this)
                continue;
            desiredSep.Normalize();
            desired += desiredSep;
            count++;
        }
        desired /= count;
        desired *= maxSpeed + speedModifier;
        Vector2 steeringForce = desired - velocity.To2d();
        steeringForce = Vector2.ClampMagnitude(steeringForce, maxForce + forceModifier);
        return steeringForce;
    }
}
