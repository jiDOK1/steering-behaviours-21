using UnityEngine;

public class Flee : SteeringBehaviour
{
    [SerializeField] float fleeDist = 5f;

    public override Vector2 Steer(
        Transform target,
        Vector3 velocity,
        float maxSpeed,
        float maxForce)
    {
        Vector2 desired = transform.position.To2d() - target.position.To2d();
        if (desired.magnitude>fleeDist)
        {
            return Vector3.zero;
        }
        desired = desired.normalized * (maxSpeed + speedModifier);
        Vector2 steeringForce = desired - velocity.To2d();
        steeringForce = Vector2.ClampMagnitude(steeringForce, maxForce + forceModifier);
        return steeringForce;
    }
}
