using UnityEngine;

public class WalkWaypoints : SteeringBehaviour
{
    [SerializeField] Transform waypointParent;
    [SerializeField] float arriveDist = 0.3f;
    Vector3[] waypoints;
    Vector3 curWaypoint;
    float timeOut = 5f;
    float timer;


    void Awake()
    {
        waypoints = new Vector3[waypointParent.childCount];
        for (int i = 0; i < waypoints.Length; i++)
        {
            waypoints[i] = waypointParent.GetChild(i).position;
        }
        curWaypoint = waypoints[Random.Range(0, waypoints.Length)];
    }

    public override Vector2 Steer(
        Transform target,
        Vector3 velocity,
        float maxSpeed,
        float maxForce)
    {
        while (Vector3.Distance(transform.position, curWaypoint) < arriveDist)
        {
            GetRandomWaypoint();
        }
        Vector2 desired = curWaypoint.To2d() - transform.position.To2d();
        desired = desired.normalized * (maxSpeed + speedModifier);
        Vector2 steeringForce = desired - velocity.To2d();
        steeringForce = Vector2.ClampMagnitude(steeringForce, maxForce + forceModifier);
        timer += Time.deltaTime;
        if (timer > timeOut)
        {
            GetRandomWaypoint();
            timer = 0f;
        }
        return steeringForce;
    }

    void GetRandomWaypoint()
    {
        curWaypoint = waypoints[Random.Range(0, waypoints.Length)];
    }
}
